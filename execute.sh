#! /bin/sh
circleci=$(circleci config validate)
PROCESS=$?

if [ $PROCESS -ne 0 ]; then
	echo $circleci
	exit
fi

echo "Config is valid!"

if circleci config process .circleci/config.yml > ciprocess.yml; then
	echo
	echo "Config is successfully processed. Output is ciprocess.yml"
	echo
else
	echo
	echo "Config cannot be processed."
	echo
	exit
fi

if circleci local execute -c ciprocess.yml --job build-apk; then
	echo
	echo "Circleci process done!"
	echo
else 
  rm -rf ciprocess.yml
  
	echo
	echo "Circleci process failed!"
	echo
	exit
fi

rm -rf ciprocess.yml
echo "Publish Successful!"
